﻿CREATE TABLE [dbo].[Categories]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Category_Name] VARCHAR(50) NOT NULL, 
    [CStatus] TINYINT NOT NULL
)

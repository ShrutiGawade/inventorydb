﻿CREATE TABLE [dbo].[Roles]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Role_Name] VARCHAR(20) NOT NULL, 
    [RStatus] TINYINT NOT NULL
)

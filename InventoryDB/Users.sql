﻿CREATE TABLE [dbo].[Users]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Person_Name] VARCHAR(50) NOT NULL, 
    [Username] VARCHAR(20) NOT NULL, 
    [Password] VARCHAR(20) NOT NULL, 
    [Phone] VARCHAR(10) NOT NULL, 
    [Email] VARCHAR(25) NOT NULL, 
    [Status] TINYINT NOT NULL, 
    [User_RoleID] INT NOT NULL FOREIGN KEY references Roles(ID) ON UPDATE CASCADE ON DELETE CASCADE
)
